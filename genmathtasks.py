import random
import argparse

def main():
	parser = argparse.ArgumentParser()
	
	parser.add_argument('-c', type=int, dest='councols', help='Count cols', default=3)
	parser.add_argument('-r', type=int, dest='countrows', help='Count rows', default=10)
	parser.add_argument('-m', type=int, dest='max', help='Max sum values in task', default=100)
	parser.add_argument('-t', choices=['-', '+', '+-', '-+'], dest='typetask', default='+-')
	args = parser.parse_args()

	for i in range(args.countrows):
		for j in range(args.councols):
			print('{0[0]:{width}{base}} {0[2]} {0[1]:{width}{base}}'.format(gettask(args.max, args.typetask), base='d', width=len(str(args.max))) + ' = ____\t\t', end="")
		print(end='\n\n')


def gettask(max = 100, type = '+-'):
	first = random.randrange(2, max-2)
	if len(type)  == 1:
		itype = type
	else:
		rtype = random.randrange(0, len(type))
		itype = type[rtype]

	if itype == '-':
		second = random.randrange(1, first)
	else:
		second = random.randrange(1, max-first, 2)
	return (first, second, itype)



if __name__ == "__main__":
	main()